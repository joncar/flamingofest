<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Main extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url');
            $this->load->helper('html');
            $this->load->helper('h');
            $this->load->database();
            $this->load->model('user');                    
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $this->load->model('querys');                                    
        }

        public function index()
        {            
            $data = array(
                'view'=>'main',
                'categorias'=>$this->querys->getCategorias()
            );
            $this->loadView($data);                
        }
        
        public function productosShow($id){
            $id = explode('-',$id);
            $id = $id[count($id)-1];
            if(is_numeric($id)){
                $this->db->where('id',$id);
                $productos = $this->querys->getProductos();
                if($productos->num_rows>0){
                    $data = array(
                        'view'=>'productos',
                        'producto'=>$productos->row()
                    );
                    $this->loadView($data);               
                }
                else{
                    throw new Exception('No se encontro el producto seleccionado','404');
                }
            }
            else{
                throw new Exception('No se encontro el producto seleccionado','404');
            }
        }

        public function success($msj)
        {
                return '<div class="alert alert-success">'.$msj.'</div>';
        }

        public function error($msj)
        {
                return '<div class="alert alert-danger">'.$msj.'</div>';
        }

        public function login()
        {
                if(!$this->user->log)
                {	
                        if(!empty($_POST['email']) && !empty($_POST['pass']))
                        {
                                $this->db->where('email',$this->input->post('email'));
                                $r = $this->db->get('user');
                                if($this->user->login($this->input->post('email',TRUE),$this->input->post('pass',TRUE)))
                                {
                                        if($r->num_rows>0 && $r->row()->status==1)
                                        {
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('main').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
                                        }
                                        else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                                }
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                        }
                        else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url('registro/index/add'));
                }
                else header("Location:".base_url('registro/index/add'));
        }

        function pages($titulo){
            $titulo = urldecode(str_replace("-","+",$titulo));
            if(!empty($titulo)){
                $pagina = $this->db->get_where('paginas',array('titulo'=>$titulo));
                if($pagina->num_rows>0){
                    $this->loadView(array('view'=>'paginas','contenido'=>$pagina->row()->contenido,'title'=>$titulo));                    
                }
                else $this->loadView('404');
            }
        }

        public function unlog()
        {
                $this->user->unlog();                
                header("Location:".site_url());
        }

        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param))
            $param = array('view'=>$param);
            $this->load->view('template',$param);
        }

        public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
        
        public function comprar(){
            header("Location:".base_url('panel'));
        }
        
        public function pagoOk(){
            $this->loadView('pagook');
        }
        
        public function pagoKo(){
            $this->loadView('pagoko');
        }

         function error404(){
            $this->loadView(array('view'=>'errors/403'));
        }
        
        function pagar_articulo($id = '',$view = 'checkout'){
            if(!empty($id)){
                $this->db->select('ventas_detalles.*, ventas.*, productos.*, ventas.precio as total, ventas_detalles.cantidad as cantidadProd, ventas_detalles.id as idDetalle');
                $this->db->join('ventas_detalles','ventas_detalles.ventas_id = ventas.id');
                $this->db->join('productos','ventas_detalles.productos_id = productos.id');
                $producto = $this->db->get_where('ventas',array('ventas.id'=>$id,'ventas.procesado'=>1));
                if($producto->num_rows>0){
                    $this->load->library('redsysapi');
                    $this->loadView(array('view'=>$view,'producto'=>$producto,'id'=>$id));
                    $_SESSION['productocompra'] = $id;
                }
                else{
                    header("Location:".base_url('usuario/compras'));
                    exit;
                }
            }
            else{
                header("Location:".base_url('usuario/compras'));
                exit;
            }
       }
       
       function pagar_articuloTest($id = '',$view = 'pagarTest'){
            if(!empty($id)){
                $this->db->select('ventas_detalles.*, ventas.*, productos.*, ventas.precio as total, ventas_detalles.cantidad as cantidadProd, ventas_detalles.id as idDetalle');
                $this->db->join('ventas_detalles','ventas_detalles.ventas_id = ventas.id');
                $this->db->join('productos','ventas_detalles.productos_id = productos.id');
                $producto = $this->db->get_where('ventas',array('ventas.id'=>$id,'ventas.procesado'=>1));
                if($producto->num_rows>0){
                    $this->load->library('redsysapi');
                    $this->loadView(array('view'=>$view,'producto'=>$producto->row(),'id'=>$id));
                    $_SESSION['productocompra'] = $id;
                }
                else{
                    header("Location:".base_url('usuario/compras'));
                    exit;
                }
            }
            else{
                header("Location:".base_url('usuario/compras'));
                exit;
            }
       }
       
       protected function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{usuarios.'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{usuarios.'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            //correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
        }
        
        function mostrarlic(){
            echo $this->db->get_where('notificaciones',array('id'=>5))->row()->texto;
        }
        
        /*function testMail(){
            $_SESSION['productocompra'] = 3983;
            $this->querys->pagoOk();
        }*/
        
        function procesarPago(){
            $this->load->library('redsysapi');
            $miObj = new RedsysAPI;            
            if (!empty($_POST)){
                $datos = $_POST["Ds_MerchantParameters"];
                $decodec = json_decode($miObj->decodeMerchantParameters($datos));                
                if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){                    
                    $id = substr($decodec->Ds_Order,3);
                    $_SESSION['productocompra'] = $id;
                    $this->querys->pagoOk();
                }else{
                    $id = substr($decodec->Ds_Order,3);
                    $_SESSION['productocompra'] = $id;
                    $this->querys->pagoNoOk();
                }
                
                
                //correo('joncar.c@gmail.com','POST',print_r($decodec,TRUE));
            }
        }
        
        public function addToCart($prod = '',$cantidad = '',$return = TRUE,$sumarcantidad = TRUE){
            if(!empty($prod) && is_numeric($prod) && $prod>0 && !empty($cantidad) && is_numeric($cantidad) && $cantidad>0){
                $this->db->where('productos.id',$prod);
                $producto = $this->querys->getProductos();
                if($producto->num_rows>0){
                    $producto = $producto->row();
                    $producto->cantidad = $cantidad;
                    $this->querys->setCarrito($producto,$sumarcantidad);
                }
            }
            if($return){
                $this->load->view('includes/fragmentos/carritonav');
            }
        }
        
        public function showCart(){
            $this->load->view('includes/fragmentos/carritonav');
        }
        
        public function delToCart($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->querys->delCarrito($prod);                
            }
            $this->load->view('includes/fragmentos/carritonav');
        }
        
        public function testCorreo(){
            correo('joncar.c@gmail.com','oagi0m0','TEST');
        }
}
