<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                if(!empty($_SESSION['carrito']) && $this->router->fetch_class()!='usuario' && $this->router->fetch_class()!='registro'){
                    header("Location:".base_url('usuario/comprar_articulo'));
                }
                
                
        }
        
        public function loadView($data)
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operación','403');
                }
                else{
                    if(!empty($data->output)){
                        $data->view = empty($data->view)?'panel':$data->view;
                        $data->crud = empty($data->crud)?'user':$data->crud;
                        $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
                    }
                    if(is_string($data)){
                        $param = array('view'=>$data);
                    }
                    $this->load->view('templateadmin',$data);
                }                
            }
        }
        
        public function index(){
            $this->loadView(array('view'=>'panel'));
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        } 
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
