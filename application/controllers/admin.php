<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Admin extends Panel {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('registrarse/add?redirect='.$_SERVER['REQUEST_URI']));
                $this->load->library('html2pdf/html2pdf');
                $this->load->library('image_crud');
                ini_set('date.timezone', 'America/Caracas');
                date_default_timezone_set('America/Caracas');   
                
                if($this->router->fetch_class()=='admin' && $_SESSION['cuenta']!=1)
                    header("Location:".base_url('panel'));
	}
       
        public function index($url = 'main',$page = 0)
	{		
              parent::index();
	}                
        
        public function loadView($data)
        {
            if(!empty($data->output)){
            $data->view = empty($data->view)?'panel':$data->view;
            $data->crud = empty($data->crud)?'user':$data->crud;
            $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */