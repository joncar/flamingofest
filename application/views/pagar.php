<?php $total = $producto->cantidad*$producto->precio ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel panel-title">
            Datos de la compra
        </h1>
    </div>
    <div class="panel-body">
        <div class="col-xs-3">
            <?= img('img/fotos_productos/'.$producto->foto,'width:100%') ?>
        </div>
        <div class="col-xs-9">
            <form action="https://sis.sermepa.es/sis/realizarPago" method="post">
            <input type="hidden" name="Ds_Merchant_MerchantCode" value="091995811">
            <?php $mensaje =  number_format($total,2,'','').base64_encode($id).'091995811'.'978'.'0'.base_url('main/procesarPago').'fdsrnb23w5nrtgmernt4'; ?>
            <input type="hidden" name="Ds_Merchant_MerchantSignature" value="<?= strtoupper(sha1($mensaje));  ?>">
            <input type="hidden" name="Ds_Merchant_Terminal" value="1">
            <input type="hidden" name="Ds_Merchant_Currency" value="978">
            <input type="hidden" name="Ds_Merchant_Amount" value="<?= number_format($total,2,'','');  ?>">
            <input type="hidden" name="Ds_Merchant_Order" value="<?= base64_encode($id);  ?>">
            <input type="hidden" name="Ds_Merchant_TransactionType" value="0">
            <input type="hidden" name="Ds_Merchant_MerchantURL" value="<?= base_url('main/procesarPago') ?>">
            <input type="hidden" name="Ds_Merchant_UrlOK" value="<?= base_url('main/pagoOk') ?>">
            <input type="hidden" name="Ds_Merchant_UrlKO" value="<?= base_url('main/pagoKo') ?>">
            
            <ul class="list-group">
                <li class="list-group-item"><b>Producto: </b><?= $producto->nombre_producto ?></li>
                <li class="list-group-item"><b>Cantidad: </b><?= $producto->cantidad ?></li>
                <li class="list-group-item"><b>Precio: </b><?= $producto->precio ?></li>
                <li class="list-group-item"><b>Total a pagar: </b><?= $producto->cantidad*$producto->precio ?></li>
                <li class="list-group-item" style="text-align: center"><button type="submit" name="procesar" class="btn btn-success">Procesar Pago</button></li>
            </ul>                
            </form>
        </div>
    </div>
</div>