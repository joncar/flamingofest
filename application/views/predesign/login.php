<?php if(empty($_SESSION['user'])): ?>
<?php if(!empty($msj))echo $msj ?>
<?php if(!empty($_SESSION['msj']))echo $_SESSION['msj'] ?>
<form role="form" class="well" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">
   <h1 align="center" style="color:black; font-size:27.9px;">Si estás registrado entra por aquí</h1>
   <?= input('email','Email','email') ?>
   <?= input('pass','Contraseña','password') ?>
   <?php if(empty($_SESSION['carrito'])): ?>
   <input type="hidden" name="redirect" value="<?= empty($_GET['redirect'])?base_url('usuario/compras'):base_url($_GET['redirect']) ?>">
   <?php else: ?>
   <input type="hidden" name="redirect" value="<?= base_url('usuario/comprar_articulo') ?>">
   <?php endif ?>
   <div align="center" style="color:black"><input type="checkbox" name="remember" value="1" checked> Recordar contraseña</div>
   <div align="center"><button type="submit" class="btn btn-success">Entrar</button>
   <!--<a class="btn btn-link" href="<?= base_url('registro/index/add') ?>">Registrate</a><br/>-->
   <a class="btn btn-link" href="<?= base_url('registro/forget') ?>">¿Olvidastes tu contraseña?</a>
   </div>
</form>
<? else: ?>
<div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large">Entrar al sistema</a></div>
<? endif; ?>
<?php $_SESSION['msj'] = null ?>
