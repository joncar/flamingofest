<?php $this->load->view('includes/headerMain') ?>

<div id="page" class="single single-product postid-1185 woocommerce woocommerce-page full-width">

    <div id="content" class="site-content">
    <section class="shop-container wcm-content-right">
            <main id="main" class="shop-main"  role="main" itemprop="mainContentOfPage" >
               <div itemscope itemtype="http://schema.org/Product" id="product-1185" class="product">
                    <div class="shop-header cl">                   
                        <nav class="woocommerce-breadcrumb" itemprop="breadcrumb">
                            <a href="<?= site_url() ?>">Home</a>&nbsp;&#47;&nbsp;
                            <a href="<?= site_url('panel') ?>">Cuenta</a>&nbsp;&#47;&nbsp;
                            <a href="#" class="showCartButton">Ver Carrito</a>&nbsp;&#47;&nbsp;
                        </nav>	
                    </div>
                   
                    <div class="shop-single-content cl">
                    <script>jQuery(document).ready(function (s) {
                    var sync1 = s("#p-preview");
                    var sync2 = s("#p-thumb");

                    sync1.owlCarousel({
                    singleItem: true,
                    slideSpeed: 1000,
                    navigation: false,
                    pagination: false,
                    autoHeight: true,
                    afterAction: syncPosition,
                    responsiveRefreshRate: 200,
                    });

                    sync2.owlCarousel({
                    items: 4,
                    itemsDesktop: [1199, 4],
                    itemsDesktopSmall: [979, 4],
                    itemsTablet: [768, 3],
                    itemsMobile: [479, 2],
                    pagination: false,
                    navigation: true,
                            navigationText: [
                                    "<i class=\"dashicons dashicons-arrow-left-alt2\"></i>",
                                    "<i class=\"dashicons dashicons-arrow-right-alt2\"></i>"
                            ],
                    responsiveRefreshRate: 100,
                    afterInit: function (el) {
                        el.find(".owl-item").eq(0).addClass("synced");
                    }
                    });

                    function syncPosition(el) {
                    var current = this.currentItem;
                    s("#p-thumb")
                        .find(".owl-item")
                        .removeClass("synced")
                        .eq(current)
                        .addClass("synced")
                    if (s("#p-thumb").data("owlCarousel") !== undefined) {
                        center(current)
                    }
                    }

                    s("#p-thumb").on("click", ".owl-item", function (e) {
                    e.preventDefault();
                    var number = s(this).data("owlItem");
                    sync1.trigger("owl.goTo", number);
                    });

                    s(".variations_form").on("change", ".variations select", function (e) {
                        e.preventDefault();
                        var number = s(this).data("owlItem");
                        sync1.trigger("owl.goTo", 0);
                    });

                    function center(number) {
                    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                    var num = number;
                    var found = false;
                    for (var i in sync2visible) {
                        if (num === sync2visible[i]) {
                                            var found = true;
                        }
                    }

                    if (found === false) {
                        if (num > sync2visible[sync2visible.length - 1]) {
                            sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                        } else {
                            if (num - 1 === -1) {
                                num = 0;
                            }
                            sync2.trigger("owl.goTo", num);
                        }
                    } else if (num === sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", sync2visible[1])
                    } else if (num === sync2visible[0]) {
                        sync2.trigger("owl.goTo", num - 1)
                    }
                    }
                    });</script>
                     
                    <div class="col-xs-12 col-sm-5">
                        <div id="p-preview" class="owl-carousel">
                            <div class="item" itemprop="image"><img class="product-slider-image" data-zoom-image="<?= base_url('img/fotos_productos/'.$producto->foto) ?>" src="<?= base_url('img/fotos_productos/'.$producto->foto) ?>" alt="" title="short-7.1" /></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <h1 itemprop="name" class="product_title entry-title"><?= $producto->nombre_producto ?></h1>
                        <div itemprop="offers">
                            <p class="price"><span class="amount" style="font-family: oswald"><?= $producto->precio ?>€</span></p>                    
                        </div>

                        <form class="cart" method="post" enctype='multipart/form-data' onsubmit="return addCart()" action="<?= base_url('main/comprar') ?>">
                            <div class="quantity">
                                <input type="number" id="cantidad" step="1" min="1"  name="cantidad" value="1" title="Qty" class="input-text qty text" size="4"  style="text-align:center"/>
                            </div>
                                <input type="hidden" id="producto" name="producto" value="<?= $producto->id ?>" />
                                <button type="submit" class="button alt">Añadir al carro</button>
                        </form>                                                

                        <div class="woocommerce-tabs">                        
                          <div class="panel entry-content" id="tab-description">
                                <button style="background: black" type="button" class="button alt">Descripción</button>
                                <p><?= $producto->descripcion ?></p>
                          </div>                   
                        </div><!-- .summary -->
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <h2 style=" font-family: raleway;font-size: 18px;text-transform: uppercase;font-weight: 800;letter-spacing: 2px;">Relacionados</h2>
                        <ul class="products">
                            <div class="relate-item">

                                <?php 
                                $this->db->where('categorias_id',$producto->categorias_id); 
                                $this->db->limit('3');
                                foreach($this->querys->getProductos()->result() as $p): ?>
                                <div class="cd-item">
                                       <div class="product-inner">
                                            <div class="product-image">
                                                <a href="<?= site_url('productos/'. toURL($p->nombre_producto).'-'.$p->id) ?>"><?= img('img/fotos_productos/'.$p->foto) ?></a>
                                            </div>
                                           <div class="product-info">
                                               <h3 class="product-title"><a title="<?= $p->nombre_producto ?>" href="<?= site_url('productos/'. toURL($p->nombre_producto).'-'.$p->id) ?>"><?= $p->nombre_producto ?></a></h3>
                                               <span class="price"><span class="amount" style="font-family: oswald;font-size: 24px"><?= $p->precio ?>€</span></span>                                               
                                            </div>
                                       </div>
                                 </div><!-- end product -->             
                                 <?php endforeach ?>
                            </div>
                        </ul>
                     </div>
                 </div>                    
                 </div><!-- #product-1185 -->          
            </main><!--- ENd Mail
    </section> <!-- .container -->
    </div><!-- #content -->   
<?php $this->load->view('includes/footer') ?>
</div><!-- #page -->