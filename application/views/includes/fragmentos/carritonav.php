<?php $carrito = $this->querys->getCarrito(); $total = 0;?>

<ul class="submenu megamenu">
    <li>
        <div class="container-fluid">
            <ul class="whishlist">
                <?php foreach($carrito as $c): ?>
                    <li style="margin-bottom:20px">
                        <div class="row" style="margin-left:0px; margin-right:0px;">
                            <div class="col-sm-2 col-xs-5">
                                <a href="<?= site_url('productos/'. toURL($c->nombre_producto).'-'.$c->id) ?>" title="">
                                    <?= img('img/fotos_productos/'.$c->foto,'width:100%;') ?>
                                </a>
                            </div>
                            <div class="col-sm-9 col-xs-5">
                                <div class="whishlist-name">
                                    <h3><a href="<?= site_url('productos/'. toURL($c->nombre_producto).'-'.$c->id) ?>" title=""><?= $c->nombre_producto ?></a> </h3>
                                </div>
                                <div class="whishlist-price">
                                    <span>Precio:</span>
                                    <strong><?= moneda($c->precio) ?></strong>                                    
                                </div>

                                <div class="whishlist-quantity">
                                    <span>Cantidad:</span>
                                    <span><?= $c->cantidad ?></span>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-1">
                                <a href="javascript:remToCart(<?= $c->id ?>)"  title="eliminar del carro"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                        <a href="javascript:remToCart('<?= $c->id ?>')" title="" class="remove">
                            <i class="icon icon-remove"></i>
                        </a>                        
                    </li>
                    <?php $total+= ($c->cantidad*$c->precio); ?>
                <?php endforeach ?>
            </ul>
            <?php if($total>0): ?>
            <div class="row" style="margin-left:0px; margin-right:0px;">
                <div class="col-xs-4 col-xs-offset-8" style="text-align:right; font-size:34px">
                    <span>Total</span>
                    <span class="price"><?= moneda($total) ?></span>
                </div>
            </div>
            <div class="row" style="margin-left:0px; margin-right:0px;">
                <div class="cart-action">
                    <a href="<?= base_url('main/comprar') ?>" title="" class="btn btn-lg btn-primary btn-block" style="background:#f196af; border-color:#f196af;">FINALIZAR COMPRA</a>
                </div>
            </div>
            <?php else: ?>
                <div class="menu-cart-total">
                    <span>Carrito Vacio</span>                    
                </div>
            <?php endif ?>
        </div>
    </li>
</ul>