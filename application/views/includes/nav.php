<nav class="menu-main-menu-container">
    <ul id="menu-main" class="cd-primary-nav">
        <li id="menu-item-267" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-267"><a href="#">Home</a>
            <ul class="sub-menu">
                    <li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-8 current_page_item menu-item-22"><a href="wordpress/">Home version 1</a></li>
                    <li id="menu-item-266" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-266"><a href="wordpress/home-2/">Home version 2</a></li>
                    <li id="menu-item-627" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-627"><a href="wordpress/home-3/">Home version 3</a></li>
            </ul>
        </li>
        <li id="menu-item-85" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-85"><a href="#">Shop</a>
            <ul class="sub-menu">
                    <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="wordpress/shop/">View All</a></li>
                    <li id="menu-item-292" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-292"><a href="wordpress/new-products/">New Products</a></li>
                    <li id="menu-item-1219" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1219"><a href="wordpress/shop-for-men/">Shop for Men</a>
                    <ul class="sub-menu">
                            <li id="menu-item-288" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-288"><a href="wordpress/product-category/men/">View All</a></li>
                            <li id="menu-item-1205" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1205"><a href="wordpress/product-category/men/shirts-coats/">Shirts &#038; Coats</a></li>
                            <li id="menu-item-1206" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1206"><a href="wordpress/product-category/men/men-pants/">Pants</a></li>
                            <li id="menu-item-277" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-277"><a href="wordpress/product-category/men/men-shoes/">Shoes</a></li>
                            <li id="menu-item-278" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-278"><a href="wordpress/product-category/men/men-accessories/">Accessories</a></li>
                    </ul>
                    </li>
                    <li id="menu-item-1226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1226"><a href="wordpress/shop-for-women/">Shop for Women</a>
                        <ul class="sub-menu">
                            <li id="menu-item-289" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-289"><a href="wordpress/product-category/women/">View All</a></li>
                            <li id="menu-item-1207" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1207"><a href="wordpress/product-category/women/women-tops/">Tops</a></li>
                            <li id="menu-item-1208" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1208"><a href="wordpress/product-category/women/women-pants/">Pants</a></li>
                            <li id="menu-item-1209" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1209"><a href="wordpress/product-category/women/women-dresses/">Dresses</a></li>
                            <li id="menu-item-283" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-283"><a href="wordpress/product-category/women/women-shoes/">Shoes</a></li>
                            <li id="menu-item-1210" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1210"><a href="wordpress/product-category/women/women-accessories/">Accessories</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-1225" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1225"><a href="wordpress/shop-for-kids/">Shop for Kids</a>
                        <ul class="sub-menu">
                                <li id="menu-item-290" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-290"><a href="wordpress/product-category/kids/">View All</a></li>
                                <li id="menu-item-286" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-286"><a href="wordpress/product-category/kids/kids-boys/">Boys</a></li>
                                <li id="menu-item-287" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-287"><a href="wordpress/product-category/kids/kids-girls/">Girls</a></li>
                                <li id="menu-item-1211" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1211"><a href="wordpress/product-category/kids/kid-accessories/">Accessories</a></li>
                        </ul>
                    </li>
            </ul>
        </li>
        <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="wordpress/blog/">Blog</a></li>
        <li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="wordpress/faqs/">FAQs</a></li>
        <li id="menu-item-229" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-229"><a href="wordpress/about-2/">About</a></li>
        <li id="menu-item-230" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-230"><a href="wordpress/contact-2/">Contact</a></li>
    </ul>
 </nav>