<footer id="colophon" class="site-footer"  role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter" >
<div class="left-footer">
    <div class="site-branding">
        <a class="site-logo" href="<?= site_url() ?>" rel="home">
            <img src="<?= base_url('img/logo.png') ?>" style="width:auto; height:auto;">
        </a>
    </div>

    <div class="bottom-info">
        <?php if(empty($_SESSION['user'])): ?>
        <div class="login-register">Hola invitado! <a id="wr-login" href="<?= site_url('panel') ?>">Entrar</a> o <a id="wr-register" href="<?= site_url('panel') ?>">Regístrate</a></div>	
        <?php else: ?>
        <div class="login-register">Hola <?= $this->user->nombre ?> <a id="wr-login" href="<?= site_url('panel') ?>">Entra a tu cuenta</a></div>	
        <?php endif ?>
    </div>
</div><!-- end left footer -->
<div class="right-footer">
    <div class="right-footer-content">
        <div class="currency">
            <ul>
                <li><a href="https://www.facebook.com/Flamingo-Fest-720179178132910/"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCudOcBEo5fnfVgB_sReYg0Q"><i class="fa fa-youtube-square"></i></a></li>
                <li><a href="https://www.instagram.com/flamingo_fest/"><i class="fa fa-instagram"></i></a></li>
                <li><a href="<?= $this->db->get('ajustes')->row()->linkedin ?>"><i class="fa fa-soundcloud"></i></a></li>
            </ul>
        </div>
        <div>
            <a href="javascript:texto()" style="
color: rgba(252, 251, 249, 1);
    text-decoration: none;
    font-weight: 300;
    font-size: 12px;
    font;
    letter-spacing: 1px;">Información Legal</a>
    </br>
            <a href="javascript:texto()" style="
color: rgba(252, 251, 249, 1);
    text-decoration: none;
    font-weight: 300;
    font-size: 12px;
    font;
    letter-spacing: 1px;"></a>
        </div>
    </div>
    <div class="right-footer-content">
        <div class="currency">
            <ul>
                <li>
                    <a href="http://www.mallorcaislandfestival.com/tienda/">
                        <img src="<?= base_url('img/banner-footer.jpeg') ?>" style="width:360px; height:95px;">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div><!-- end right footer -->
</div><!-- end more inner footer -->
</footer><!-- #colophon -->
<div id="mask-blur"></div>
<div id="modal" style="background:rgba(0,0,0,.9); width:100%; height:100%; position:fixed; top:0px; left:0px; padding:50px; display:none; z-index:10000">
<div style="background:white; height:auto; padding:20px; overflow:auto; max-height:600px;">
    <div align="right" style="font-size:24px;">      	
    <a href="javascript:closeModal()" style="font-size: 18px; color: #f196af;">Seguir comprando</a>
  </div>
  <div id="contenidoModal">Contenido</div>
</div>
</div>
<div id='texto'>
    <div id="contenidoModal" style="color:#d9534f">DATOS FISCALES</div>www.flamingofest.cat es un portal web propiedad de FINALIA VIAJES S.L, 
     </br>CIF B65847170 inscrita en el Registro Mercantil de Barcelona tomo 43305, folio 158, hoja B-424111, INS.1. El C.I.C.M.A. de Agencia de Viajes de la Comunidad de Madrid es 2256.</br></br>
     
    <div id="contenidoModal"style="color:#d9534f">PRIVACIDAD</div>En cumplimiento de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal ("LOPD") y en la Ley 34/2002, de 11 de julio, 
    de Servicios de la Sociedad de la Información ("LSSI"), le recordamos que sus datos personales están incorporados y son tratados en nuestros ficheros, 
    con el fin de poderle prestar y ofrecer nuestros servicios y darles trámite (gestión de reservas y del cobro de los productos y servicios adquiridos), 
    enviarle información y publicidad sobre ofertas, promociones y recomendaciones, así como para realizar encuestas, estadísticas y análisis de tendencias de mercado. 
    
    Usted puede ejercer sus derechos de acceso, rectificación, cancelación y oposición al tratamiento de sus datos personales, así como revocar su consentimiento para el 
    envío de comunicaciones comerciales, notificándolo al Responsable del Fichero en su domicilio, en Gran Via de les Corts Catalanes 439, 7/2. 08015 Barcelona (ESPAÑA) o 
    puedes cursar tu baja mediante correo electrónico a <a href="mailto:info@finalia.es">info@finalia.es</a>.  </br></br>
    
    <div id="contenidoModal"style="color:#d9534f">SERVICIOS DE CONTRATACIÓN POR INTERNET</div>
	Ciertos contenidos de las websites de FINALIA VIAJES S.L. contienen la posibilidad de contratación por Internet. El uso de los mismos requerirá la lectura 
	y aceptación obligatoria de las condiciones generales de contratación establecidas al efecto por FINALIA VIAJES S.L.</br></br>
	
	   <div id="contenidoModal"style="color:#d9534f">CONDICIONES DE CANCELACIÓN</div>
	La totalidad del viaje se efectúa a través de tres pagos. Se ofrece al cliente la posibilidad de contratar 
	adicionalmente al precio del viaje el seguro de cancelación por motivos académicos y/o hospitalarios por 35€. La contratación 
	de este seguro de cancelación sólo se podrá hacer efectiva con anterioridad 02/febrero/2017. Con el seguro de cancelación por motivos 
	académicos y/o hospitalarios el cliente tiene derecho a la cancelación de su plaza con derecho íntegro a la devolución del 100% del importe del segundo 
	y tercer pago notificándolo por correo electrónico a <a href="reservas@flamingofest.com">reservas@flamingofest.com</a> con anterioridad al 01/Junio/2017 adjuntando el justificante que 
	certifique los siguientes casos que se incluyen en el derecho de devolución: ingreso hospitalario del cliente en fechas del viaje, impedimento físico 
	o psicológico de no poder realizar el viaje, fallecimiento de un familiar hasta tercer grado, repetición del curso académico que actualmente 
	imparta el cliente o abandono de los estudios. Cancelaciones que se produzcan sin haber contratado el seguro de cancelación sólo se reembolsará 
	el cuarto pago de la reserva siempre que sea notificado por correo electrónico a <a href="reservas@flamingofest.com">reservas@flamingofest.com</a> con anterioridad 
	al 01/Junio/2017. Todas las cancelaciones posteriores al 01/Junio/2017 incurrirán en 100% gastos de cancelación. Para cualquier 
	reembolso del importe de la reserva se deberá devolver las participaciones entregadas antes del 01/Junio/2017 a la siguiente dirección 
	C/ Girona nº34 en Igualada 08700, de lo contrario se descontará del importe a reembolsar. La fecha de envío del correo electrónico 
	será válida para comprobar la fecha de cancelación.
</div>


<div id='legal'>
    
</div>
<script>
function addCart(){
    var prod = $("#producto").val();
    var cant = $("#cantidad").val();
    $.get('<?= base_url('main/addToCart') ?>/'+prod+'/'+cant,{},function(data){
        $("#contenidoModal").html(data);
        $("#modal").show();
    });
    return false;
}

function showCart(){
    $.get('<?= base_url('main/showCart') ?>',{},function(data){
        $("#contenidoModal").html(data);
        $("#modal").show();
    });
    return false;
}

function remToCart(id){
    $.get('<?= base_url('main/delToCart') ?>/'+id,{},function(data){
        $("#contenidoModal").html(data);
        $("#modal").show();
    });    
}

function closeModal(){
    $("#modal").hide();
}

function texto(){
    emergente($("#texto").html());
}

function legal(){
    emergente($("#legal").html());
}

$(document).on('click','.showCartButton',function(){
   showCart();
});
</script>
