<div id="cd-nav" class="cd-nav">
    <div class="cd-navigation-wrapper">
        <div class="cd-half-block">
            <?php $this->load->view('includes/nav'); ?>
        </div><!-- .cd-half-block -->

        <div class="cd-half-block">					
            <div class="cd-contact-info">
                    Address: R39 David Building, London, G3 NYC <br />
                    Email: <a href="mailto:mail@ninja.com">mail@ninja.com</a> <br />
                    Website: www.flamingofest.com			
            </div>
        </div> <!-- .cd-half-block -->
    </div> <!-- .cd-navigation-wrapper -->
</div> <!-- .cd-nav -->