<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>            
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-user"></i>
                        <span class="menu-text">Usuario</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li>
                        <a href="<?= base_url('usuario/compras') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Compras</span>                                    
                        </a>                                
                    </li>                   
                </ul>
            </li>
            <?php if($this->user->getAccess('grupos.*',array('grupos.id'=>1))->num_rows>0): ?>
            <!--- Admin --->
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-lock"></i>
                        <span class="menu-text">Admin</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li>
                        <a href="<?= base_url('admin/ajustes') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Ajustes</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/ventas') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Ventas</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/categorias') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Categorias</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/productos') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Productos</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/notificaciones') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Notificaciones</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/provincias') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Provincias</span>                                    
                        </a>                                
                    </li>
                    <!--- Seguridad --->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Seguridad</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('seguridad/grupos') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Grupos</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/funciones') ?>">
                                    <i class="menu-icon fa fa-arrows"></i>
                                    <span class="menu-text">Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/user') ?>">
                                    <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">Usuarios</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>
                    <!--- Fin seguridad ---->
                </ul>
            </li>
            <!--- Fin Admin ---->
            <?php endif ?>
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
