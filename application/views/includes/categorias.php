<ul>
    <?php foreach($categorias->result() as $n=>$c): ?>
    <li class="cd-single-project is-loaded">
        <style>
            .projects-container .cd-single-project::after {
                background-position:left center;
            }

            .projects-container .cd-single-project:nth-of-type(<?= $n+1 ?>):after {
                    background-image: url(<?= base_url()?>img/fotos_categorias/<?= $c->foto ?>);
            }
            .projects-container .cd-single-project:nth-of-type(<?= $n+1 ?>):before {
                content: "<?= base_url()?>img/fotos_categorias//<?= $c->foto ?>";
            }
            .projects-container .cd-single-project:nth-of-type(<?= $n+1 ?>):before {
                    content: "<?= base_url()?>img/fotos_categorias//<?= $c->foto ?>";
            }
            <?php if($n==0): ?>
            .project-3-col .cd-single-project:nth-of-type(<?= $n+2 ?>) {
                    top:44.333333333333%;
            }
            @media only screen and (min-width: 1025px) {
                    .projects-container.project-3-col .cd-single-project:nth-of-type(<?= $n+2 ?>) {
                        top: 0;
                        left:33vw;
                    }
            }
            <?php elseif($n==1): ?>
            .project-3-col .cd-single-project:nth-of-type(3) {
                        top:88.666666666667%;
             }
             @media only screen and (min-width: 1025px) {
                 .projects-container.project-3-col .cd-single-project:nth-of-type(3) {
                        top: 0;
                        left:66vw;
                  }
             }
             <?php else: ?>
                  .project-3-col .cd-single-project:nth-of-type(4) {
                       top:100%;
                  }
                  @media only screen and (min-width: 1025px) {
                   .projects-container.project-3-col .cd-single-project:nth-of-type(4) {
                           top: 0;
                           left:75vw;
                   }
               }
             <?php endif ?>
        </style>
        <div class="cd-title">
            <?= $c->nombre_categoria ?>            
        </div> <!-- .cd-title -->

        <div class="cd-project-info">
            <div class="product-for-cat">
                    <div class="products cd-items cd-container">
                            <?php 
                                $this->db->where('categorias_id',$c->id); 
                                foreach($this->querys->getProductos()->result() as $p): 
                            ?>
                            <div class="product cd-item">
                                <div class="product-inner">
                                    <div class="product-image">
                                        <a href="<?= site_url('productos/'. toURL($p->nombre_producto).'-'.$p->id) ?>">
                                        <?= img('img/fotos_productos/'.$p->foto,'width:') ?>
                                        </a>
                                    </div>
                                    <div class="product-info">
                                        <h3 class="product-title">
                                            <a title="<?= $p->nombre_producto ?>" href="<?= site_url('productos/'. toURL($p->nombre_producto).'-'.$p->id) ?>">
                                                <?= $p->nombre_producto ?>
                                            </a>
                                        </h3>
                                        <span class="price">
                                            <span class="amount"style="font-family: oswald; font-size: 32px"><?= $p->precio ?>€</span>
                                        </span>                                        
                                        <div class="product-action cl">
                                            <a href="#0" class="cd-quickview-trigger"><i class="fa fa-eye"></i> 
                                                <span>Vista Previa</span>
                                            </a>
                                            <a href="<?= site_url('productos/'. toURL($p->nombre_producto).'-'.$p->id) ?>" rel="nofollow" data-product_id="1034" data-product_sku="999" class="button add_to_cart_button product_type_simple"><i class="fa fa-shopping-cart"></i> <strong>Comprar</strong></a>
                                        </div>			
                                        <div class="cd-hide-info">
                                            <h3 class="product-title">
                                                <a href="<?= site_url('productos/'. toURL($p->nombre_producto).'-'.$p->id) ?>">
                                                    <?= $p->nombre_producto ?>
                                                </a>
                                            </h3>
                                            <span class="price"><span class="amount" style=" font-family: oswald"><?= $p->precio ?>€</span></span>
                                            <div class="description" itemprop="description">
                                                <p style="font-family: raleway; font-weight: 300"><?= substr(strip_tags($p->descripcion),0,155).'...' ?></p>
                                            </div>
                                            <div class="cd-item-action cl">
                                                <a href="<?= site_url('productos/'. toURL($p->nombre_producto).'-'.$p->id) ?>" rel="nofollow" data-product_id="1034" data-product_sku="999" class="button add_to_cart_button product_type_simple"><i class="fa fa-shopping-cart"></i> <strong>Comprar</strong></a>                                                                       
                                            </div> <!-- cd-item-action -->
                                        </div> <!-- cd-hide-info -->
                                    </div>                                                            
                                </div>                                                        
                            </div><!-- end product -->
                            <?php endforeach ?>
                    </div>                                            
            </div> <!-- .cd-project-info -->
        </li>
    <?php endforeach ?>
</ul>
