<?php $this->load->view('includes/headerMain') ?>

    <div id="page" class="hfeed site" >
        <div id="content" class="site-content" style="background: black">	
            <section class="home-page-container">
            <style type="text/css">
                    .project-3-col .cd-single-project{
                            height: 44.333333333333%;
                    }
                    @media only screen and (min-width: 1025px) {
                            .project-3-col .cd-single-project {
                                    width: 34vw;
                                    height: 100%;
                            }
                            .projects-container.project-3-col .cd-single-project.is-full-width{
                                    /*left:  !important;*/
                            }
                    }
            </style>
            
            <main id="main" class="site-main"  role="main" itemprop="mainContentOfPage" >
                <div class="projects-container project-3-col">
                    <?php $this->load->view('includes/categorias') ?>
                    <div class="cd-quick-view">
                            <div class="cd-slider-wrapper">
                                    <ul class="cd-slider">
                                            <li class="selected">
                                                <img src="wordpress/wp-content/themes/ninja/assets/img/item-1.jpg" alt="Product 1">
                                            </li>
                                    </ul> <!-- cd-slider -->
                            </div> <!-- cd-slider-wrapper -->

                            <div class="cd-item-info">

                            </div> <!-- cd-item-info -->
                            <a href="#0" class="cd-quickview-close">X</a>
                    </div> <!-- cd-quick-view -->
                    <div class="quickview-mask"></div>
                        <a href="#0" class="cd-close">Close</a>
                        <a href="#0" class="cd-scroll">Scroll</a>
                    </div> <!-- .project-container -->
            </main><!-- #main -->

            </section><!-- .container -->

        </div><!-- #content -->

        <?php $this->load->view('includes/footer') ?>

    </div><!-- #page -->
