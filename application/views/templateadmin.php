<!Doctype html>
<html lang="es">
        <head>
                <title><?= empty($title)?'Flamingo Fest':$title ?></title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <link rel='stylesheet' id='ninja-google-font-Raleway-css'  href='https://fonts.googleapis.com/css?family=Oswald:700' type='text/css' media='all' />
                <link rel='stylesheet' id='ninja-google-font-Raleway-css'  href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C300%2C400%2C600%2C700%2C800%2C900&#038;subset=latin%2Cgreek-ext%2Ccyrillic%2Clatin-ext%2Cgreek%2Ccyrillic-ext%2Cvietnamese&#038;ver=4.2.4' type='text/css' media='all' />
                <link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>">
                <?php 
                if(!empty($css_files) && !empty($js_files)):
                foreach($css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
                <?php endforeach; ?>
                <?php foreach($js_files as $file): ?>
                <script src="<?= $file ?>"></script>
                <?php endforeach; ?>                
                <?php endif; ?>
                <?php if(empty($crud) || empty($css_files)): ?>
                <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
                <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
                <?php endif ?>
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">                
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/ace.min.css') ?>">
                <script src="<?= base_url('js/ace-extra.min.js') ?>"></script>	
                <script src="<?= base_url().'js/frame.js' ?>"></script>
                <?php $this->load->view('predesign/multiselect') ?>
                <style>
                    .btn-success:hover, .btn-success:focus, .btn-success.focus, .btn-success:active, .btn-success.active, .open > .dropdown-toggle.btn-success {
                        background-color: black !important;
                        border-color: black !important;
                        color: #ffffff;
                    }
                    .btn-success:hover, .btn-success:active, .open > .btn-success.dropdown-toggle {
                         background-color: black !important;
                        border-color: black !important;
                        color: #ffffff;
                    }
                    body, #page {
                        background-color: #efefef;
                        color: #353533;
                    }
                    .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6, p{
                        font-family:'Oswald', sans-serif;
                    }
                    label, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"]{
                        font-family:'Raleway', sans-serif;
                    }
                </style>
        </head>
        <?php if($this->router->fetch_class()=='registro'): ?>
        <body class="no-skin" style="background:url(<?= base_url('img/fondo_login.jpg') ?>) no-repeat; background-size:; background-attachment:fixed;">
         <?php else: ?>
            <body class="no-skin" >
          <?php endif ?>
                <?php $this->load->view('includes/header') ?>
                <div class="main-container" id="main-container" >
                        <?php $this->load->view('includes/sidebar') ?>
                        <div class="main-content">
                            <div class="main-content-inner">
                                <?php $this->load->view('includes/breadcum') ?>
                                <div class="page-content">						
                                    <?php $this->load->view($view) ?>                                            
                                </div><!-- /.page-content -->
                            </div>
                        </div><!-- /.main-content -->			
                </div><!-- /.main-container -->
                <script src="<?= base_url("js/ace.min.js") ?>"></script>
                <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>	
                <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
        </body>
</html>
