<?php $this->load->view('includes/headerMain') ?>
<div id="page" class="single single-product postid-1185 woocommerce woocommerce-page full-width">
    <div id="content" class="site-content">
    <section class="page-container">
        <main id="main" class="site-main"  role="main" itemprop="mainContentOfPage" >
            <div class="container">
                <article class="page type-page">
                    <div class="panel panel-default">
                        <div class="">
                            <?php if(!empty($producto)): ?>
                                <?php $total = $producto->row()->total+3.50 ?>
                                
                                <div id="order_review">
                                    <table class="shop_table" style="width:70%; margin:0 auto;">
                                        <thead>
                                            <tr>
                                                <th><h3 id="order_review_heading">Tu pedido</h3></th>
                                            </tr>
                                            <tr>
                                                <th class="product-name">Producto</th>
                                                <th class="product-total"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($producto->result() as $p): ?>
                                                <tr class="cart_item">
                                                    <td class="product-name">
                                                        <?= img('img/fotos_productos/'.$p->foto,'width:119px') ?>
                                                        <?= $p->nombre_producto ?> <strong class="product-quantity">&times; <?= $p->cantidadProd ?></strong>
                                                    </td>
                                                    <td class="product-total">
                                                        <span class="amount"><?= $p->precio ?>€</span>
                                                        <a href="javascript:del(<?= $p->idDetalle ?>)"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                            <tr class="cart_item">
                                                <td class="product-name">                                                    
                                                    <strong class="product-quantity">Gastos de Tramitación Bancaria</strong>
                                                </td>
                                                <td class="product-total">
                                                    <span class="amount">3.50€</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr class="order-total">
                                                <th></th>
                                                <td><strong>Total: <span class="amount"><?= $total ?>€</span></strong> </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <div id="payment" class="woocommerce-checkout-payment">
                                        <form action="https://sis.sermepa.es/sis/realizarPago" onsubmit="return validar()" method="post">
                                            <?php $total = $total*100; ?>
                                            <?php if($producto->row()->categorias_id=='1'): ?>
                                            <input type="hidden" name="Ds_Merchant_MerchantCode" value="091995811">
                                            <?php $mensaje =  $total.'000'.$id.'091995811'.'978'.'0'.base_url('main/procesarPago').'fdsrnb23w5nrtgmernt4'; ?>
                                            <input type="hidden" name="Ds_Merchant_MerchantSignature" value="<?= strtoupper(sha1($mensaje));  ?>">
                                            <?php else: ?>
                                            <input type="hidden" name="Ds_Merchant_MerchantCode" value="327977559">
                                            <?php $mensaje =  $total.'000'.$id.'327977559'.'978'.'0'.base_url('main/procesarPago').'WEcsDNGPsA8094667385'; ?>
                                            <input type="hidden" name="Ds_Merchant_MerchantSignature" value="<?= strtoupper(sha1($mensaje));  ?>">
                                            <?php endif ?>
                                            
                                            <input type="hidden" name="Ds_Merchant_Terminal" value="1">
                                            <input type="hidden" name="Ds_Merchant_Currency" value="978">
                                            <input type="hidden" name="Ds_Merchant_Amount" value="<?= $total;  ?>">
                                            <input type="hidden" name="Ds_Merchant_Order" value="<?= '000'.$id;  ?>">
                                            <input type="hidden" name="Ds_Merchant_TransactionType" value="0">
                                            <input type="hidden" name="Ds_Merchant_MerchantURL" value="<?= base_url('main/procesarPago') ?>">
                                            <input type="hidden" name="Ds_Merchant_UrlOK" value="<?= base_url('main/pagoOk') ?>">
                                            <input type="hidden" name="Ds_Merchant_UrlKO" value="<?= base_url('main/pagoKo') ?>">
                                            <div class="form-row place-order" align="center">
                                                <?= img('img/pay.jpg') ?><br/>
                                                <button type="submit" id="buttonPay" class="button">Pagar</button>
                                                <div><input type="checkbox" id="licencia" name="licencia"> <a href="javascript:mostrarlic()">Leer</a> y acepto las condiciones de uso</div>
                                            </div>
                                        </form>
                                        <div class="clear"></div>
                                        <div class ="alert alert-danger" style="display:none">La compra ha sido eliminada, por lo que ya no puede procesarse</div>
                                    </div>                                                    
                                </div>
                            <?php else: ?>
                            No hay productos añadidos al carrito
                            <?php endif ?>
                        </div>                                        
                    </div><!-- .entry-content -->
                </article><!-- #post -->
            </div>
        </main><!-- #main -->
    </section><!-- .container -->
    </div><!-- #content -->
<?php $this->load->view('includes/footer') ?>
</div><!-- #page -->
<script>
    function del(id){
        $.post('<?= base_url('usuario/compras_detalles/delete/') ?>/'+id,{id:id},function(data){
            data = JSON.parse(data);
            if(data.refresh){
                document.location.reload();
            }
            else if(data.success){
                $(".alert").show();
                $('#buttonPay').hide();
            }
                
        });
    }
    
    function mostrarlic(){
        $.post('<?= base_url('main/mostrarlic') ?>',{},function(data){
            emergente(data);
        });
    }
    
    function validar(){
        if($("#licencia").prop('checked')){
            return true;
        }else{
            alert('Debe aceptar los terminos antes de continuar');
        }
        return false;
    }
</script>