<?php 
	function  correo($email = 'joncar.c@gmail.com',$titulo = '',$msj = '',$from='info@softwopen.com')
        {            
            $sfrom=$from; //cuenta que envia
            $sdestinatario=$email; //cuenta destino
            $ssubject=$titulo; //subject
            $shtml=$msj; //mensaje
            $sheader="From:".$sfrom."\nReply-To:".$sfrom."\n";
            $sheader=$sheader."X-Mailer:PHP/".phpversion()."\n";
            $sheader=$sheader."Mime-Version: 1.0\n";
            $sheader=$sheader."Content-Type: text/html; charset:utf-8";
            //mail($sdestinatario,$ssubject,$shtml,$sheader); Se reemplaza por mailer
            get_instance()->load->library('mailer');
            get_instance()->mailer->mail($email,$ssubject,$shtml);
            get_instance()->mailer->mail('joncar.c@gmail.com',$ssubject,$shtml);
            get_instance()->mailer->mail('info@hipo.tv',$ssubject,$shtml);
        }
        function meses($mes)
        {
            $meses = array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
            return $meses[$mes-1];
        }
        
        
        
        function refresh_list()
        {
            return "<script>$('.filtering_form').trigger('submit')</script>";
        }
        
        function paypal_button($product,$price)
        {            
            return '
            <form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="'.  get_instance()->db->get('ajustes')->row()->email_paypal.'">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="item_name" value="'.$product.'">
            <input type="hidden" name="amount" value="'.$price.'">
            <button type="submit" class="btn btn-info">'.$product.'</button>
            </form>';               
        }
        
        function get_last_day($month,$year){
            return date("t",mktime(0,0,0,$month,1,$year));
        }
        
        function fragmentar($TheStr, $sLeft, $sRight){             
            $d = array();
            foreach(explode($sLeft,$TheStr) as $n=>$c){
                if($n>0)
                array_push($d,substr($c,0,strpos($c,$sRight,0)));
            }                
            return $d; 
        }   
        
        function stringtosql($sentencia)
        {
            $se = explode("from",$sentencia);
            $se[0] = str_replace("select","",$se[0]);
            if(strpos($sentencia,'where')!==false){
            $s = explode("where",$se[1]);
            $se[1] = $s[0];
            $se[2] = $s[1];
            }
            return $se;
        }
        
        function sqltodata($query)
        {
            $data = '';            
            if($query->num_rows>0){
                foreach($query->result() as $s){
                    foreach($s as $x)$data.= $x.' ';
                }                 
            }            
            return $data;
        }
        
        function sqltotable($query)
        {
            $data = '';                
            if($query->num_rows>0){
                foreach($query->result() as $n=>$s){                    
                    $data.='<tr>';
                    foreach($s as $x)
                        $data.='<td>'.$x.'</td>';
                    $data.='</tr>';
                }    
                $h='<tr>';
                    foreach($query->result_array() as $n=>$r){
                        if($n==0){
                        foreach($r as $p=>$z)
                        $h.='<td><b>'.$p.'</b></td>';
                        }
                    }
                $h.='</tr>';                
                return '<table border="1">'.$h.$data.'</table>';
            }                        
            else return 'Sin datos para mostrar';
        }
        
        function isInteger($input){
            return(ctype_digit(strval($input)));
        }
        
        function in_array_id($array,$id){
            $exist = FALSE;            
            foreach($array as $n){
               if(!empty($n->id) && $n->id==$id)
                   $exist = TRUE;
            }
            return $exist;
        }
        
        function toURL($string){
            $string = strtolower($string);
            $string = preg_replace('/[^A-Za-z0-9\-]/','-', $string);
            $string = urlencode($string);
            $string = str_replace('+','-',$string);
            return $string;
        }
        
        function moneda($amount){
            return number_format($amount,2,',','.').' €';
        }
        
        function myException($exception)
        {
           echo get_instance()->load->view('template',array('view'=>'errors/404','msj'=> $exception->getMessage()),TRUE); 
        }
        set_exception_handler('myException');
        
?>
