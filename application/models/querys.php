<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    var $referidos = array();
    
    function __construct()
    {
        parent::__construct();
    } 
    
    function getCategorias(){
        return $this->db->get('categorias'); 
    }
    
    function getProductos(){
        $this->db->order_by('priority','ASC');
        return $this->db->get('productos'); 
    }
    
    function pagoOk(){
        if(!empty($_SESSION['productocompra'])){                
            $this->db->select('ventas.precio as totalP, user.*, ventas.*, productos.*, ventas_detalles.cantidad as cantidadProd, ventas_detalles.id as idDetalle');
            $this->db->join('ventas_detalles','ventas_detalles.ventas_id = ventas.id');
            $this->db->join('productos','ventas_detalles.productos_id = productos.id');
            $this->db->join('user','user.id = ventas.user_id','inner');
            $producto = $this->db->get_where('ventas',array('ventas.id'=>$_SESSION['productocompra']));
            if($producto->num_rows>0){                
                $this->enviarcorreo($producto->row(),3);
                $this->enviarcorreo($producto->row(),4,'reservas@flamingofest.com');
            }
            $this->db->update('ventas',array('procesado'=>2),array('id'=>$_SESSION['productocompra']));
            unset($_SESSION['productocompra']);
        }
    }
    
    function pagoNoOk(){
        if(!empty($_SESSION['productocompra'])){                            
            $this->db->update('ventas',array('procesado'=>-1),array('id'=>$_SESSION['productocompra']));
            unset($_SESSION['productocompra']);
        }
    }
    
    protected function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
        $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();        
        foreach($usuario as $n=>$v){
         $mensaje->texto = str_replace('{usuarios.'.$n.'}',$v,$mensaje->texto);
         $mensaje->titulo = str_replace('{usuarios.'.$n.'}',$v,$mensaje->titulo);   
        }            
        if(empty($destinatario)){
            correo($usuario->email,$mensaje->titulo,$mensaje->texto);
        }
        else{
            correo($destinatario,$mensaje->titulo,$mensaje->texto);
        }
    }
    
    function getCarrito(){
        if(empty($_SESSION['carrito'])){
            return array();
        }
        else{
            return $_SESSION['carrito'];
        }
    }
    
    function setCarrito($item,$sumarcantidad = TRUE){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }
        $existente = false;
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$item->id){
                if($sumarcantidad){
                    $_SESSION['carrito'][$n]->cantidad+=$item->cantidad;
                }else{
                    $_SESSION['carrito'][$n]->cantidad=$item->cantidad;
                }
                $existente = true;
            }
        }
        if(!$existente){
            array_push($_SESSION['carrito'],$item);
        }        
    }
    
    function delCarrito($producto_id){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }        
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$producto_id){
                unset($_SESSION['carrito'][$n]);
            }
        }                
    }
}
?>
