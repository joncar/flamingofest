<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Usuario extends Panel{
        
        const NOPROCESADO = '-1';
        const PORPROCESAR = '1';
        const PROCESADO = '2';
        
        function __construct() {
            parent::__construct();
        }
        
        function compras(){
            $this->as = array('compras'=>'ventas');
            $crud = $this->crud_function('','');
            $crud->where('user_id',$this->user->id);
            $crud->unset_add()
                    ->unset_edit()
                    ->unset_read()
                    ->unset_export();
            $crud->unset_columns('user_id');            
            $crud->display_as('productos_id','Producto');
            $crud->columns('id','productos','precio','cantidad','fecha_compra','procesado');
            $crud->display_as('id','Nro. Compra')
                 ->display_as('user_id','Usuario');
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<a href="'.site_url('main/pagar_articulo/'.$row->id).'" class="label label-default">Por procesar</a>'; break;
                    case '2': return '<span class="label label-success">Procesado</span>'; break;
                }
            });
            $crud->callback_before_delete(function($primary){
                $producto = get_instance()->db->get_where('ventas',array('id'=>$primary));
                if($producto->num_rows>0){
                    if($producto->row()->procesado!=1){
                        return false;
                    }
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function compras_detalles($x = '',$y = ''){
            $this->as = array('compras_detalles'=>'ventas_detalles');
            $venta =  $this->db->get_where('ventas_detalles',array('id'=>$y));
            if($venta->num_rows>0){
            get_instance()->dbdeleteid = $venta->row()->ventas_id;
            $crud = $this->crud_function('','');
            $crud->unset_add()
                     ->unset_list()
                     ->unset_edit()
                    ->unset_read()
                    ->unset_print()
                    ->unset_export();
            $crud->callback_before_delete(function($primary){
                $ventas_detalles = get_instance()->db->get_where('ventas_detalles',array('id'=>$primary));
                if($ventas_detalles->num_rows>0){
                    $ventasTotales = get_instance()->db->get_where('ventas_detalles',array('ventas_id'=>$ventas_detalles->row()->ventas_id));
                    if($ventasTotales->num_rows==1 && $ventasTotales->row()->id==$primary){
                        get_instance()->db->delete('ventas',array('id'=>$ventas_detalles->row()->ventas_id));
                        return true;
                    }else{
                        return true;
                    }
                }
                else{
                    return false;
                }
            });
            
            $crud->callback_after_delete(function(){
                $ventas = get_instance()->db->get_where('ventas',array('id'=>get_instance()->dbdeleteid));
                if($ventas->num_rows>0){
                    echo json_encode(array('success'=>true,'refresh'=>true));
                    exit;
                }else{
                    return true;
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
            }else{return false;}
        }
        
        function comprar_articulo(){
            if(!empty($_SESSION['carrito'])){
                $precio = 0;
                $cantidad = 0;
                $productos = '';
                $carrito = $this->querys->getCarrito();
                foreach($carrito as $c){
                    $precio+= ($c->precio*$c->cantidad);
                    $cantidad+= $c->cantidad;
                    $productos.= $c->nombre_producto.' ';
                }
                $this->db->insert('ventas',array('cantidad'=>$cantidad,'productos'=>$productos,
                    'user_id'=>$this->user->id,
                    'fecha_compra'=>date("Y-m-d H:i:s"),
                    'precio'=>$precio,
                    'cantidad'=>$cantidad,
                    'procesado'=>self::PORPROCESAR));
                $id = $this->db->insert_id();
                foreach($carrito as $c){
                    $this->db->insert('ventas_detalles',array(
                        'ventas_id'=>$id,
                        'productos_id'=>$c->id,
                        'cantidad'=>$c->cantidad,
                        'precio'=>$c->precio
                    ));
                }
                unset($_SESSION['carrito']);
                header("Location:".base_url('main/pagar_articulo/'.$id));
            }
            /*
            $precio = $_SESSION['cantidad'] * $this->db->get_where('productos',array('id'=>$_SESSION['producto']))->row()->precio;
            $this->db->insert('ventas',array('cantidad'=>$_SESSION['cantidad'],'productos_id'=>$_SESSION['producto'],'user_id'=>$this->user->id,'fecha_compra'=>date("Y-m-d H:i:s"),'precio'=>$precio,'procesado'=>self::PORPROCESAR));
            $producto = $_SESSION['producto'];
            unset($_SESSION['producto']);
            unset($_SESSION['cantidad']);
            header("Location:".base_url('main/pagar_articulo/'.$this->db->insert_id()));-*/
        }
        
         function pagar_articulo($id = ''){
            if(!empty($id)){
                $this->db->where('ventas.id',$id);
                $this->db->where('ventas.procesado',1);
                $this->db->join('ventas','ventas.productos_id = productos.id','inner');
                $producto = $this->querys->getProductos();
                if($producto->num_rows>0){
                    $this->loadView(array('view'=>'pagar','producto'=>$producto->row(),'id'=>$id));
                }
                else{
                    header("Location:".base_url('usuario/compras'));
                    exit;
                }
            }
            else{
                header("Location:".base_url('usuario/compras'));
                exit;
            }
       }
    }
?>
