/**
 * @version    1.3
 * @package    Ninja
 * @author     WooRockets Team <support@woorockets.com>
 * @copyright  Copyright (C) 2014 WooRockets.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://www.woorockets.com
 */

(function($) {
	"use strict";
	$(document).ready(function() {

		/*  [ Detecting Mobile Devices ]
		- - - - - - - - - - - - - - - - - - - - */
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
			},
			Desktop: function() {
				return window.innerWidth <= 960;
			},
			any: function() {
				return ( isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() || isMobile.Desktop() );
			}
		};

		/*  [ add animation main menu ]
		- - - - - - - - - - - - - - - - - - - - */
		$('#menu-main > li').each(function(index){
			$(this).addClass('animation-element fast');
		});
		$('.cd-contact-info').addClass('animation-element fast');

		$('.cd-navigation-wrapper .animation-element').each(function(index){
			$(this).attr('data-delay', index*200 + 300);
		});

		/*  [ Click show/hide sub menu ]
		- - - - - - - - - - - - - - - - - - - - */
		$(document).on( 'click', '#menu-main > li.menu-item-has-children > a', function(event){
			event.preventDefault();
			$( '#menu-main > li > a' ).not( this ).parent().find( '> .sub-menu' ).slideUp();
			$(this).parent().find('> .sub-menu').slideToggle();
		});

		/*  [ Tabs right footer ]
		- - - - - - - - - - - - - - - - - - - - */
		$(document).on( 'click', '.more-inner-footer .tab li', function(event){
			event.preventDefault();
			var selector = $(this).find('a').attr('href');
			$(this).parent().find('a').not(this).removeClass('active');
			$(this).find('a').addClass('active');
			setTimeout(function(){
				$(selector).find('form input[type="text"]').focus();
			}, 700);

			if($('.has-children-show').length){		
				$(this).parents('.more-inner-footer').find('.tab-content').not(selector).fadeOut(100).removeClass('in-right');
				$(this).parents('.more-inner-footer').find(selector).fadeIn().addClass('in-right');
			}else{
				$(this).parents('.more-inner-footer').css('top', "-"+$(selector).outerHeight()+"px");
				$(this).parents('.more-inner-footer').addClass('has-children-show');
				$(this).parents('.more-inner-footer').find('.tab-content').not(selector).fadeOut(100);
				$(this).parents('.more-inner-footer').find(selector).addClass('in-right').fadeIn(100);
			};
			
			$('#mask-blur').fadeIn(100);
		});
		$('#mask-blur').click(function(){
			$('.more-inner-footer').css('top', 0).removeClass('has-children-show');
			$('.more-inner-footer .tab-content').hide().removeClass('out-left in-right');
			$(this).fadeOut(100);
			$('.more-inner-footer .tab li a').removeClass('active');
		});

		if($('#search-box form input[type="text"]').val() != ''){
			$('#search-box form label').hide();
		}
		$('#search-box form input[type="text"]').keydown(function(){
			$(this).parent().find('label').hide();
		});
		$('#search-box form input[type="text"]').blur(function(){
			if($(this).val() == ''){
				$(this).parent().find('label').show();
			}			
		});

		/*  [ Set height main content ]
		- - - - - - - - - - - - - - - - - - - - */
		if($('#wpadminbar').length){
			$('html').css('height', $(window).height() - $('#wpadminbar').outerHeight() + 'px');
			$('.site-content, .shop-sidebar, .primary-sidebar, .secondary-sidebar').css('height', $(window).height() - $('#wpadminbar').outerHeight() - $('.site-footer').outerHeight() + 'px');
		}
		else{
			$('.site-content, .shop-sidebar, .primary-sidebar, .secondary-sidebar').css('height', $(window).height() - $('.site-footer').outerHeight() + 'px');
		}
		$(window).resize(function(){
			if($('#wpadminbar').length){
				$('html').css('height', $(window).height() - $('#wpadminbar').outerHeight() + 'px');
				$('.site-content, .shop-sidebar, .primary-sidebar, .secondary-sidebar').css('height', $(window).height() - $('#wpadminbar').outerHeight() - $('.site-footer').outerHeight() + 'px');
			}
			else{
				$('.site-content, .shop-sidebar, .primary-sidebar, .secondary-sidebar').css('height', $(window).height() - $('.site-footer').outerHeight() + 'px');
			}
		});

		/*  [ product related responsive ]
		- - - - - - - - - - - - - - - - - - - - */
		if($(window).width() <= 1024){
			$('.woocommerce .product-related').appendTo('.woocommerce .shop-single-content');
		}
		$(window).resize(function(){
			if($(window).width() <= 1024){
				$('.woocommerce .product-related').appendTo('.woocommerce .shop-single-content');
			}else{
				$('.woocommerce .product-related').appendTo('.woocommerce .summary');
			}
		});

		/*  [ Required comment form ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.comment-form input.submit' ).on( 'click', function() {
			var author = $( this ).parents( '.comment-form' ).find( 'input#author' ),
				email = $( this ).parents( '.comment-form' ).find( 'input#email' ),
				comment = $( this ).parents( '.comment-form' ).find( 'textarea#comment' ),
				filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if( author.val() == "" ) {
				alert('Please enter your name!');
				author.focus();
				return false;
			}
			if( email.val() == "" ) {
				alert('Please enter your email!');
				email.focus();
				return false;
			}
			if (!filter.test(email.val())) {
				alert('Please enter a valid email address!');
				email.focus();
				return false;
			}
			if( comment.val() == "" ) {
				comment.focus();
				alert('Please enter your comment!');
				return false;
			}
		});

		/*  [ Remove product in cart ]
		- - - - - - - - - - - - - - - - - - - - */
		$('body').on('click','.cart-item-container .remove', function(e) {
			e.preventDefault();
			var cart_item_key = $(this).attr("data-product_id");
			var parents_product = $(this).parents('li')
			parents_product.children('.cart-item-thumbnail').addClass('loading');

			$.ajax({
				type: 'POST',
				url: yith_wcwl_plugin_ajax_web_url,
				data: { 
					action: "product_remove", 
					cart_item_key: cart_item_key
				},
				success: function(val){
					if( val ) {
						val = $.parseJSON(val);
							
						if( val.count_product == 0 ) {
							$( '.widget_shopping_cart_content .product_list_widget' ).html( '<li class="empty">No products in the cart.</li>' );
							$( '.widget_shopping_cart_content .total' ).remove();
							$( '.widget_shopping_cart_content .buttons' ).remove();
						} else {
							parents_product.remove();
							$( '.widget_shopping_cart_content .total .amount' ).html( val.price_total );
						}
						
						$( '.more-inner-footer .tab .cart-control span' ).html( val.count_product );
					}	       
				}
			});
			return false;
	   });
		
		/*  [ Category parent page ]
		- - - - - - - - - - - - - - - - - - - - */
		if($('.cat-has-child-wrap')){
			$('.cat-has-child-wrap').parents('.site-main').addClass('site-main-cat-has-child');
		}

		/*  [ Ajax single page add to cart ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.single_add_to_cart_button' ).on( 'click', function(e) {
			e.preventDefault();

			var _this = $( this ),
				qty = $( this ).parents( 'form' ).find( '.quantity .qty' ),
				product_id = $( this ).parents( 'form' ).find( 'input[name="add-to-cart"]' );

			_this.addClass( 'loading' );

			$.ajax({
				type: 'POST',
				url: yith_wcwl_plugin_ajax_web_url,
				data: { 
					action: "single_ajax_add_to_cart",
					qty: qty.val(), 
					product_id: product_id.val()
				},
				success: function(val){
					if( val ) {
						val = $.parseJSON(val);
							
						if( val.count_product == 0 ) {
							$( '.widget_shopping_cart_content .product_list_widget' ).html( '<li class="empty">No products in the cart.</li>' );
							$( '.widget_shopping_cart_content .total' ).remove();
							$( '.widget_shopping_cart_content .buttons' ).remove();
						} else {
							$( '.widget_shopping_cart_content .product_list_widget' ).html( val.list_product );

							if( $( '.widget_shopping_cart_content .total' ).length > 0 ) {

								$( '.widget_shopping_cart_content .total .amount' ).html( val.price_total );

							} else {
								$( '.widget_shopping_cart_content' ).append( val.cart_actions );
							}
						}
						
						$( '.more-inner-footer .tab .cart-control span' ).html( val.count_product );

						_this.removeClass( 'loading' ).addClass( 'added' );
						_this.parent().append( val.view_cart );
					}
				}
			});
		});

		/*  [ Remove p empty tag of page builder ]
		- - - - - - - - - - - - - - - - - - - - */
		$( 'p' ).each(function() {
			var $this = $( this );
				if( $this.html().replace(/\s|&nbsp;/g, '').length == 0) {
				$this.remove();
			}
		});		

		/*  [ Custom add to cart button ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.add_to_cart_button' ).click(function( e ) {
			setTimeout(function() {
				$(this).siblings('.added_to_cart').remove();
				$(this).removeClass('.added');
			}.bind(this), 350);
		});

		/*  [ Custom row fullwidth ]
		- - - - - - - - - - - - - - - - - - - - */
		$('.wr_fullwidth').each(function() {
			var $self_html = $(this).html();
			$(this).empty();$(this).append('<div class="container">' + $self_html + '</div>');
		});
		$( 'body' ).removeClass( 'wr-full-width' );

	});

	$(window).load(function() {
		/*  [ Page loader]
		- - - - - - - - - - - - - - - - - - - - */
		setTimeout(function() {
			$( 'body' ).addClass( 'loaded' );
			setTimeout(function () {
				$('#pageloader').remove();
			}, 1000);
		}, 1000);

		/*  [ Set height childrent post item ]
		- - - - - - - - - - - - - - - - - - - - */
		function height_child_of_post(){
			var img_max_height = 0;
			var content_max_height = 0;
			$('.site-main .post').each(function(){
				var img_height = $(this).find('.entry-thumb > a > img').outerHeight();
				var content_height = $(this).find('.entry-content').outerHeight();
				if(img_height > img_max_height){
					img_max_height = img_height;
				}
				if(content_height > content_max_height){
					content_max_height = content_height;
				}
			});
			$('.site-main .post .entry-thumb iframe, .site-main .post .entry-thumb').css('height', img_max_height);
			// $('.site-main .post .entry-content').css('height', content_max_height);
		}
		height_child_of_post();
		$(window).resize(function(){
			height_child_of_post();
		});

		/*  [ put featured product up the first ]
		- - - - - - - - - - - - - - - - - - - - */
		$('.products .product.featured').each(function(){
			$(this).prependTo($(this).parent());
		});

		function product_featured(){
			var featured_fist = $('.products .product.featured').first();
			var product_next = $(featured_fist).next().height();
			var featured_info_w = $(featured_fist).find('.product-info').outerHeight();
			if(product_next > 0){
				setTimeout(function(){
					$(featured_fist).find('.product-image').css({
						height: (product_next*2 - featured_info_w + 19),
					});
				}, 100);			
			}
		};

		setTimeout(function(){
			product_featured();	
		}, 1000);		

		$(window).resize(function(){
			var featured_fist = $('.products .product.featured').first();
			var product_next = $(featured_fist).next().height();
			var featured_info_w = $(featured_fist).find('.product-info').outerHeight();
			if(product_next > 0){
				$(featured_fist).find('.product-image').css({
					height: (product_next*2 - featured_info_w + 19),
				});
			}
		});

		/*  [ Advance filter shop page ]
		- - - - - - - - - - - - - - - - - - - - */
		$(document).on( 'click', '.filter-title', function(){
			$(this).parents('.site-content').find('.shop-sidebar').addClass('show').parent().addClass('margin');
			setTimeout( function() {
				product_featured();
			}, 800);
		});
		$(document).on( 'click', '.filter-close', function(){
			$(this).parents('.site-content').find('.shop-sidebar').removeClass('show').parent().removeClass('margin');
			setTimeout( function() {
				product_featured();
			}, 800);
		});
		$(document).on( 'click', '.yith-woo-ajax-navigation ul li a', function(){
			$( '.woocommerce .yith-wcan-loading' ).css('height', $(window).height() - $('.site-footer').outerHeight() + 'px');
		});

		/*  [ Pagination ajax load more ]
		- - - - - - - - - - - - - - - - - - - - */
		$( '.page-numbers li a.next' ).parent().addClass('wr-pagination-reamore').find( 'a.next' ).html( 'Load more' );

		var pagi_text = ["1"];

		$('.site-content').on('click', '.page-numbers a', function(e){
	        e.preventDefault();
	        var link = $(this).attr('href');
	        var current_content = $(this).parents('.site-content').find('.post-wrap').children();

	        $('body').addClass('pagination-loading');

	        $('.page-numbers').load(link + ' .page-numbers >', function(){
	        	pagi_text.forEach(function(entry) {
				    $('.page-numbers li').eq(entry).find('a').parent().html('<span>' + entry + '</span>');
				});

				pagi_text.push($('.page-numbers li .current').text());
				$( '.page-numbers li a.next' ).parent().addClass('wr-pagination-reamore').find( 'a.next' ).html( 'Load more' );
	        });

	        $('.post-wrap').load(link + ' .post-wrap >', function() {
	        	var content = $(this);
	        	setTimeout(function(){
	        		$(content).prepend( $( current_content ) );
            		height_child_of_post();
                	$(".site-content").animate({scrollTop: $(".page-numbers").offset().top}, 700);
            	}, 50);
	        	setTimeout(function(){
	        		$('body').removeClass('pagination-loading');
	            }, 700);
	        });
	    });
	});
})(jQuery);